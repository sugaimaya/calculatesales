package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.lst";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String OVER_SUM = "合計金額が10桁を超えました";
	private static final String FILE_NOT_SERIALNUMBR= "売上ファイル名が連番になっていません";
	private static final String INVALID_FORMAT= "のフォーマットが不正です";
	private static final String INVALID_BRANCHCODE="の支店コードが不正です";
	private static final String INVALID_PRODUCTCODE="の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */


	public static void main(String[] args) {
		//コマンドライン引数が1つ設定されていない場合エラー表示
		if (args.length !=1){
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> productNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> productSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,"[0-9]{3}","支店")) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_OUT, productNames, productSales,"^[A-Za-z0-9]+${8}","商品")) {
		return;
		}
		//args[0]には引数で指定したC:\\Users\\sugai.maya\\Desktop\\売上集計課題が入っている

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		//全てのファイルを取得する
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();


		for(int i = 0; i <files.length ; i++) {
			String str = files[i].getName();

			//売上ファイルの判定
			if( files[i].isFile() && str.matches("^[0-9]{8}.+rcd$")) {
				rcdFiles.add(files[i]); //売上ファイルの条件に当てはまるものをListに追加
			}
		}

		//売上ファイルが連番かどうか確認
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size()-1; i++){

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			if((latter - former) != 1){
				System.out.println(FILE_NOT_SERIALNUMBR);
				return;
			}
		}

		BufferedReader br = null;

		//売上ファイルを1ファイルずつ取得
		for(int i = 0; i <rcdFiles.size() ; i++) {

			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				List<String> fileContents = new ArrayList<>();


				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
					fileContents.add(line); //lineの中身を1行ずつ追加(保持)
				}

				//売上ファイルの中身が4行以上ならばエラー表示
				if(fileContents.size()!= 3){
					System.out.println(rcdFiles.get(i).getName() + INVALID_FORMAT);
					return;
				}

				//支店コードが支店定義ファイルになかった場合エラー表示
				if(!branchNames.containsKey(fileContents.get(0))){
					System.out.println(rcdFiles.get(i).getName() + INVALID_BRANCHCODE);
					return;
				}

				//商品コードが商品定義ファイルになかった場合エラー表示
				if(!productNames.containsKey(fileContents.get(1))){
					System.out.println(rcdFiles.get(i).getName() + INVALID_PRODUCTCODE);
					return;
				}

				//売上ファイルが数字でなければエラー表示
				if(!fileContents.get(2).matches("^[0-9]*$")){
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(fileContents.get(2)); //売上金額Stringからlongに変換
				System.out.println(fileSale);

				Long branchSaleAmount = branchSales.get(fileContents.get(0)) + fileSale; //既にMapにある売上金額を取得し、売上ファイルの金額に加算
				System.out.println(branchSaleAmount);

				Long productSaleAmount = productSales.get(fileContents.get(1)) + fileSale; //既にMapにある売上金額を取得し、売上ファイルの金額に加算

				//合計金額が10桁を超えた場合のエラー
				if((branchSaleAmount >= 10000000000L)||(productSaleAmount >= 10000000000L)){
					System.out.println(OVER_SUM);
					return;
				}
				branchSales.put(fileContents.get(0), branchSaleAmount);
				productSales.put(fileContents.get(0), productSaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;

					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, productNames, productSales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル・商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales,String regex,String error) {
		//brという箱の作成（中身はまだなにも入っていない）
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//支店定義ファイル・商品定義ファイルが存在しない場合エラー表示
			if(!file.exists()) {
				System.out.println(error+FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file); //ファイルを開けられるように準備
			br = new BufferedReader(fr);          //ファイルを開く


			String line;
			// 一行ずつ支店定義ファイル・商品定義ファイル内の読み込み
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				String[] items = line.split(",");

				//支店定義ファイル・商品定義ファイルの仕様が満たされていない場合エラー表示
				if((items.length != 2) || (!items[0].matches(regex))){

					System.out.println(error+FILE_INVALID_FORMAT);
					return false;
				}

				System.out.println(line);
				System.out.println(items[0]);
				System.out.println(items[1]);

				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;

	}

	/**
	 * 支店別集計ファイル・商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		//支店別集計ファイル・商品別集計ファイルの作成
		try {
			File file = new File(path,fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for (String key : Sales.keySet()) {
				System.out.println(key);
				System.out.println(Sales.get(key));
				System.out.println(Names.get(key));

				//支店別集計ファイル・商品別集計ファイルへの出力
				bw.write(key+","+Names.get(key)+","+Sales.get(key));
				bw.newLine();
			}

		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
